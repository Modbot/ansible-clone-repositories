Modbot Clone Repositories Ansible Role
=========

This role accepts a list of dictionaries containing the name, url, dest and version of a git repo, and clones them accordingly.

Installs
------------

N/A

Requirements
------------

N/A

Role Variables
------------

N/A

Dependencies
------------

N/A
